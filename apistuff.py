import os, re, requests, socket
from datetime import datetime
from dotenv import load_dotenv

# If IP address given in IP_list.txt is followed by space and number like '193.1.122.11 50'
# script scrapes IP addresses from 193.1.122.11 to 193.1.122.50.
# Feel free to add your API key in .env file
# api_key = 'your_api_key'
# pip install -r requirements.txt

load_dotenv()
API_KEY = os.environ.get('api_key')
parent_dir = os.getcwd()


class Api_data:
    def __init__(self, query: str, type: str, api_key: str=''):
        self.query = query
        self.type = type
        self.api_key = api_key
        if not self.api_key:
            self.api_response = requests.get(f'https://api.hackertarget.com/{self.type}/?q={self.query}')
        else:
            self.api_response = requests.get(f'https://api.hackertarget.com/{self.type}/?q={self.query}&apikey={self.api_key}')


    def getData(self) -> str:
        return self.api_response.text


def writeToCSV(path: str, content: str, id: str):
    try:
        output_file = open(path, 'w', encoding='utf8')
        output_file.write(content)
        output_file.close()
        writeToLogs(f'{id} - Results saved in {path}')
    except:
        writeToLogs(f'{id} - Failed to save results')

def writeToLogs(content: str):
    date = str(datetime.now())[:-7]
    logs = open('logs.txt', 'a', encoding='utf8')
    logs.write(date + ' - ' + content + '\n')
    logs.close()


def scrapeIPs(input_path: str) -> None:
    '''
    Writes to .csv domains of IPs written in input file.
    Uses API
    '''
    input_file = open(input_path, 'r', encoding='utf8')
    for line in input_file.readlines():
        ip = line.replace('\n', '').strip()
        if not ip:
            continue
        try:
            to_element = re.search(' ([0-9][0-9]*)', ip).group(0)
        except:
            to_element = None
        if to_element:
            ip = ip.replace(to_element, '')
            to_element = to_element.strip()
            from_element = re.findall('\d+|$', ip)[-2]
            if int(from_element) > int(to_element):
                temp = to_element
                to_element = from_element
                from_element = temp
                ip = ip.replace(to_element, from_element)
            for i in range(int(from_element), int(to_element)+1):
                new_ip = str(i).join(ip.rsplit(from_element, 1))
                api = Api_data(new_ip, 'reverseiplookup', API_KEY)
                if re.search('No DNS A records found', api.getData()):
                    writeToLogs(f'{new_ip} - No DNS A records found')
                    del api
                    continue
                output_path = f'{parent_dir}/IP_dump/IP-{new_ip}.csv'
                writeToCSV(output_path, api.getData(), new_ip)
                del api
        else:
            api = Api_data(ip, 'reverseiplookup', API_KEY)
            if re.search('No DNS A records found', api.getData()):
                writeToLogs(f'{ip} - No DNS A records found')
                del api
                continue
            output_path = f'{parent_dir}/IP_dump/IP-{ip}.csv'
            writeToCSV(output_path, api.getData(), ip)
            del api
    input_file.close()

def scrapeDomains(input_path: str, use_api: bool) -> None:
    '''
    Writes to .csv IPs of domains written in input file.
    '''
    input_file = open(input_path, 'r', encoding='utf8')
    domainsContent = []
    if use_api:
        for line in input_file.readlines():
            domain = line.replace('\n', '').strip()
            if not domain:
                continue
            api = Api_data(domain, 'dnslookup', API_KEY)
            if re.search('error input invalid', api.getData()):
                writeToLogs(f'{domain} - Invalid domain given')
                del api
                continue

            ip = re.search('[0-9].*', api.getData()).group(0)
            domainsContent.append(ip)

            del api
    else:
        file_content = []
        for line in input_file.readlines():
            file_content.append(line.strip())
        domainsContent = getIPsFromDomains(file_content)
    output_path = f'{parent_dir}/domain_dump/Domains.csv'
    writeToCSV(output_path, ';'.join(domainsContent).replace('\n;', '\n'), 'Domains')
    input_file.close()
    
def getIPsFromDomains(domain_list: list) -> list:
    ips = []

    for line in domain_list:
        line = line.replace('\n', '').strip()
        if not line:
            continue
        try:
            x = f'{line};{socket.gethostbyname(line)}'
            # x = socket.gethostbyname(line)
        except:
            x = f'{line};Failed to get IP'
        ips.append(x + '\n')
    return ips


