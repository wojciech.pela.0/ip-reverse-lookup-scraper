import os
import dearpygui.dearpygui as dpg
from apistuff import *

# If IP address given in IP_list.txt is followed by space and number like '193.1.122.11 50'
# script scrapes IP addresses from 193.1.122.11 to 193.1.122.50.
# Feel free to add your API key in .env file
# api_key = 'your_api_key'
# pip install -r requirements.txt


def clearField(sender, app_data, user_data) -> None:
    dpg.set_value(user_data, '')

def saveToFileAndRun(sender, app_data, user_data) -> None:
    Gui.progressHandler(1)
    element = user_data[0]
    path_to_file = user_data[1]
    content = []
    file = open(path_to_file, 'w', encoding='utf8')
    unfiltered_content = dpg.get_value(element).split('\n')
    for line in unfiltered_content:
        if line:
            content.append(line)
    file.write('\n'.join(content))
    file.close()
    if user_data[2]=='ip':
        scrapeIPs(path_to_file)
    elif user_data[2]=='domain':
        scrapeDomains(path_to_file, False)
    Gui.progressHandler(2)


def getFromFile(path: str) -> None:
    try:
        file = open(path, 'r', encoding='utf8')
        unfiltered_content = file.readlines()
        file.close()
        content = []
        for line in unfiltered_content:
            if line.strip('\n'):
                content.append(line)
    except:
        return None
    return ''.join(content)

class Gui:
    def __init__(self):
        dpg.create_context()
        self.ips = dpg.generate_uuid()
        self.domains = dpg.generate_uuid()
        global progressShower
        progressShower = dpg.generate_uuid()
        self.ips_file_path = f'{parent_dir}/IP_list.txt'
        self.domains_file_path = f'{parent_dir}/domain_list.txt'
        dpg.create_viewport(title='Hacker Target API Scraper', width=600, height=600, resizable=False)

        with dpg.window(label="Interface", width=dpg.get_viewport_width(), height=dpg.get_viewport_height(), no_move=True, no_collapse=True, no_title_bar=True, no_resize=True):
            dpg.add_text('DNS Host Records Scraper')
            dpg.add_text('Domains:')
            dpg.add_input_text(label="", default_value=getFromFile(self.domains_file_path), tag=self.domains, multiline=True)
            dpg.add_button(label='Clear domain list', callback=clearField, user_data=self.domains)
            dpg.add_text(f'IP list text file path:\n{self.domains_file_path}')
            dpg.add_button(label='Start script', callback=saveToFileAndRun, user_data=[self.domains, self.domains_file_path, 'domain'])
            dpg.add_text('IP Reverse Lookup Scraper')
            dpg.add_text('IP addresses:')
            dpg.add_input_text(label="", default_value=getFromFile(self.ips_file_path), tag=self.ips, multiline=True)
            dpg.add_button(label='Clear IP list', callback=clearField, user_data=self.ips)
            dpg.add_text(f'IP list text file path:\n{self.ips_file_path}')
            dpg.add_button(label='Start script', callback=saveToFileAndRun, user_data=[self.ips, self.ips_file_path, 'ip'])
            dpg.add_text('', tag=progressShower)

        with dpg.theme() as global_theme:
            with dpg.theme_component(dpg.mvAll):
                dpg.add_theme_color(dpg.mvThemeCol_FrameBg, (58, 38, 24), category=dpg.mvThemeCat_Core)
                dpg.add_theme_color(dpg.mvThemeCol_Button, (99, 55, 57), category=dpg.mvThemeCat_Core)
                dpg.add_theme_color(dpg.mvThemeCol_WindowBg, (23,22,20), category=dpg.mvThemeCat_Core)
                dpg.add_theme_color(dpg.mvThemeCol_Text, (204, 204, 204), category=dpg.mvThemeCat_Core)
                dpg.add_theme_style(dpg.mvStyleVar_FrameRounding, 5, category=dpg.mvThemeCat_Core)
            with dpg.theme_component(dpg.mvInputInt):
                dpg.add_theme_color(dpg.mvThemeCol_FrameBg, (255, 255, 20), category=dpg.mvThemeCat_Core)
                dpg.add_theme_style(dpg.mvStyleVar_FrameRounding, 5, category=dpg.mvThemeCat_Core)

        dpg.bind_theme(global_theme)
        dpg.setup_dearpygui()
        dpg.show_viewport()
        dpg.start_dearpygui()
        dpg.destroy_context()

    def progressHandler(val: int) -> None:
        if val==0:
            dpg.set_value(progressShower, '')
        if val==1:
            dpg.set_value(progressShower, 'Scraping...')
        if val==2:
            dpg.set_value(progressShower, 'Done')


if __name__=='__main__':
    if not os.path.exists(f'{parent_dir}/IP_dump'):
        os.makedirs(f'{parent_dir}/IP_dump')
    if not os.path.exists(f'{parent_dir}/domain_dump'):
        os.makedirs(f'{parent_dir}/domain_dump')

    try:
        input_file_ips = open(f'{parent_dir}/IP_list.txt', 'r', encoding='utf8')
        input_file_domains = open(f'{parent_dir}/domain_list.txt', 'r', encoding='utf8')
    except:
        input_file_ips = open(f'{parent_dir}/IP_list.txt', 'w+', encoding='utf8')
        input_file_domains = open(f'{parent_dir}/domain_list.txt', 'w+', encoding='utf8')
        input_file_ips.write('')
        input_file_domains.write('')
    interface = Gui()